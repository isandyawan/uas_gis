// import d3.legend.js;

queue()
.defer(d3.json, "data/banten.json")
.await(Data_loading);

var pvg;
var provinces;
var Dataprov;
var fieldname;
var dataById= {};
var color, range, maxValue, minValue;
var tooltip;
var selectedData;
var skala;

function init(){
	doAjax('services/listnmfield.php','q=', 'funcselfield', 'post', '1')
}

function ShowMap(){
	selectedData = getValue('selfield');
	switch(selectedData){
		case "Jumlah Perusahaan Industri":
			queue()
			.defer(d3.csv, "data/jumlahperusahaan.csv")
			.await(Data_loading2);
		break;
		case "Jumlah Tenaga Kerja Perusahaan":
			queue()
			.defer(d3.csv, "data/tenagakerja.csv")
			.await(Data_loading2);
		break;
		case "Tingkat Kemiskinan Pendudukan":
			queue()
			.defer(d3.csv, "data/pendmiskin.csv")
			.await(Data_loading2);
		break;
		case "Produk Domestrik Bruto Atas Harga Konstan":
			queue()
			.defer(d3.csv, "data/pdrb.csv")
			.await(Data_loading2);
		break;
		default :
		queue()
			.defer(d3.csv, "data/pdrb.csv")
			.await(Data_loading2);
		break;
	}
	
	document.getElementById('judul').innerHTML = getValue('selfield');
	
	
}

function Data_loading(error, prov){
	// init();

	vartooltip = d3.select('body').append('div')
.attr('class', 'hidden tooltip');

if (error) throw error;
var width = 800, height=350;
svg= d3.select("#map").append("svg")
.attr("width", width)
.attr("height", height)
.attr("id","baseMap")
svg.append("rect")
.attr("class", "background")
.attr("width", width)
.attr("height", height)
.attr("transform", "translate(0,-70)").on("click", clicked);

var projection = d3.geo.mercator()
.center([106, -6.4])
.scale(12000)
.translate([width -width/2.2, height/3.])
var path = d3.geo.path()
.projection(projection);

var zoom = d3.behavior.zoom()
.translate([0, 0])
.scale(1)
.scaleExtent([1, 8])
.on("zoom", zoomed);

provinces = svg.append("g").attr("class", "features");
provinces.selectAll("path")
.data(topojson.feature(prov, prov.objects.subunits).features)
.enter().append("path")
.attr("d", path)
.attr("class", 'feature')

.on('mousemove', function(d) {
var mouse = d3.mouse(svg.node()).map(function(d) {
return parseInt(d);
});
tooltip.classed('hidden', false)
.attr('style', 'left:' + (mouse[0] + 50) +
'px; top:' + (mouse[1] +150) + 'px')
.html( function (){
if (dataById[d.properties.KABKOTNO] > 0) {
var number= parseFloat(dataById[d.properties.KABKOTNO]);
return d.properties.KABKOT + " = " + number.toLocaleString(['ban', 'id']);
}else{
return (d.properties.KABKOT);
}
});
})
.on('mouseout', function() {
tooltip.classed('hidden', true);
});

provinces.append("path")
.datum(topojson.mesh(prov, prov.objects.subunits, function(a, b) { return a !== b; }))
.attr("class", "boundary")
.attr("d", path);




svg.call(zoom)
.call(zoom.event);

}

function Data_loading2(error, data1){
	if (error) throw error;
	Dataprov= data1;
	
	fieldname = 2014;
	dataById= {};
	Dataprov.forEach(function(d) {
		if (d[fieldname] > 0){
			dataById[d.KABKOTNO] = d[fieldname];
		}
	});
	tooltip = d3.select('body').append('div')
	.attr('class', 'hidden tooltip');
	ChoroMap();
}

function zoomed() {
	
	provinces.style("stroke-width", 1.5 / d3.event.scale + "px");
	provinces.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	d3.select("#skala").text("Skala  1:"+Math.round(1000000/d3.event.scale));
	// document.getElementById("skala")=d3.event.scale;
}

function SetData(data){

	range = (ss.jenks(data.map(function(d) {
		console.log(data);
		console.log("JENK");
		console.log(d);
		return +d[fieldname]; }), 3))

	minValue= ss.min(data.map(function(d) {
		if (d[fieldname]>0){
			return +d[fieldname];
		}
	}))
	maxValue= ss.max(data.map(function(d) {
		if (d[fieldname]>0){
			return +d[fieldname];
		}
	}));
}

function CleanMapChor(feat){
	document.getElementById('legend').innerHTML = "";
	d3.selectAll(feat)
	.transition()
	.duration(500)
	.style("fill", function(d) {
		return "#ccc";
	});
}

function ChoroMap(){
d3.svg.legend = function() {
    
    var legendValues=[{color: "red", stop: [0,1]},{color: "blue", stop: [1,2]},{color: "purple", stop: [2,3]},{color: "yellow", stop: [3,4]},{color: "Aquamarine", stop: [4,5]}];
    var legendScale;
    var cellWidth = 30;
    var cellHeight = 20;
    var adjustable = false;
    var labelFormat = d3.format(".01f");
    var labelUnits = "units";
    var lastValue = 6;
    var changeValue = 1;
    var orientation = "horizontal";
    var cellPadding = 0;
   
    function legend(g) {
    
    function cellRange(valuePosition, changeVal) {
    legendValues[valuePosition].stop[0] += changeVal;
    legendValues[valuePosition - 1].stop[1] += changeVal;
    redraw();
    }

    function redraw() {
        
        g.selectAll("g.legendCells").data(legendValues).exit().remove();
        g.selectAll("g.legendCells").select("rect").style("fill", function(d) {return d.color});
        if (orientation == "vertical") {
            g.selectAll("g.legendCells").select("text.breakLabels").style("display", "block").style("text-anchor", "start").attr("x", cellWidth + cellPadding).attr("y", 5 + (cellHeight / 2)).text(function(d) {return labelFormat(d.stop[0]) + (d.stop[1].length > 0 ? " - " + labelFormat(d.stop[1]) : "")})
            g.selectAll("g.legendCells").attr("transform", function(d,i) {return "translate(0," + (i * (cellHeight + cellPadding)) + ")" });
        }
        else {
            g.selectAll("g.legendCells").attr("transform", function(d,i) {return "translate(" + (i * cellWidth) + ",0)" });
            g.selectAll("text.breakLabels").style("text-anchor", "middle").attr("x", 0).attr("y", -7).style("display", function(d,i) {return i == 0 ? "none" : "block"}).text(function(d) {return labelFormat(d.stop[0])});
        }
    }
    g.selectAll("g.legendCells")
    .data(legendValues)
    .enter()
    .append("g")
    .attr("class", "legendCells")
    .attr("transform", function(d,i) {return "translate(" + (i * (cellWidth + cellPadding)) + ",0)" })
    
    g.selectAll("g.legendCells")
    .append("rect")
    .attr("height", cellHeight)
    .attr("width", cellWidth)
    .style("fill", function(d) {return d.color})
    .style("stroke", "black")
    .style("stroke-width", "2px");

    g.selectAll("g.legendCells")
    .append("text")
    .attr("class", "breakLabels")
    .style("pointer-events", "none");
    
    g.append("text")
    .text(labelUnits)
    .attr("y", -7);
    
    redraw();
    }
    
    legend.inputScale = function(newScale) {
        if (!arguments.length) return scale;
            scale = newScale;
            legendValues = [];
            if (scale.invertExtent) {
                //Is a quantile scale
                scale.range().forEach(function(el) {
                    var cellObject = {color: el, stop: scale.invertExtent(el)}
                    legendValues.push(cellObject)
                })
            }
            else {
                scale.domain().forEach(function (el) {
                    var cellObject = {color: scale(el), stop: [el,""]}
                    legendValues.push(cellObject)
                })
            }
            return this;
    }
    
    legend.scale = function(testValue) {
        var foundColor = legendValues[legendValues.length - 1].color;
        for (el in legendValues) {
            if(testValue < legendValues[el].stop[1]) {
                foundColor = legendValues[el].color;
                break;
            }
        }
        return foundColor;
    }

    legend.cellWidth = function(newCellSize) {
        if (!arguments.length) return cellWidth;
            cellWidth = newCellSize;
            return this;
    }

    legend.cellHeight = function(newCellSize) {
        if (!arguments.length) return cellHeight;
            cellHeight = newCellSize;
            return this;
    }

    legend.cellPadding = function(newCellPadding) {
        if (!arguments.length) return cellPadding;
            cellPadding = newCellPadding;
            return this;
    }
    
    legend.cellExtent = function(incColor,newExtent) {
        var selectedStop = legendValues.filter(function(el) {return el.color == incColor})[0].stop;
        if (arguments.length == 1) return selectedStop;
            legendValues.filter(function(el) {return el.color == incColor})[0].stop = newExtent;
            return this;
    }
    
    legend.cellStepping = function(incStep) {
        if (!arguments.length) return changeValue;
            changeValue = incStep;
            return this;
    }
    
    legend.units = function(incUnits) {
        if (!arguments.length) return labelUnits;
            labelUnits = incUnits;
            return this;
    }
    
    legend.orientation = function(incOrient) {
        if (!arguments.length) return orientation;
            orientation = incOrient;
            return this;
    }

    legend.labelFormat = function(incFormat) {
        if (!arguments.length) return labelFormat;
            labelFormat = incFormat;
            if (incFormat == "none") {
                labelFormat = function(inc) {return inc};
            }
            return this;
    }

return legend;    
    
}

	SetData(Dataprov);
	CleanMapChor(".feature");
	color = d3.scale.threshold()
	.domain(range)
	.range(["#71f787", "#2bc644", "#15af2e"]);
	console.log(range);
	d3.selectAll(".feature")
	.transition()
	.delay(function(d, i) {
		return i* 10;
	})
	.duration(250)
	.style("fill", function(d) {
		if (dataById[d.properties.KABKOTNO] > 0){
			
		console.log(color(dataById[d.properties.KABKOTNO]));
			return color(dataById[d.properties.KABKOTNO]);
		}else{
			return "#ccc";
		}
	});



sampleThreshold=d3.scale.threshold().domain(range).range(["#71f787", "#2bc644", "#15af2e"]);
  horizontalLegend = d3.svg.legend().units("").cellWidth(80).cellHeight(10).inputScale(sampleThreshold).cellStepping(100);
  d3.select("#legend").append("judul").html("LEGENDA").attr("class","legend-title");
  d3.select("#legend").append("div").attr("id","skala");
  d3.select("#legend").append("svg").attr("id", "legenda").append("g").attr("transform", "translate(20,40)").attr("class", "legend").call(horizontalLegend);
  

}

function clicked(d) {
  var x, y, k;

  if (d && centered !== d) {
    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 4;
    centered = d;
  } else {
    x = width / 2;
    y = height / 2;
    k = 1;
    centered = null;
  }

  g.selectAll("path")
      .classed("active", centered && function(d) { return d === centered; });

  g.transition()
      .duration(750)
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
      .style("stroke-width", 1.5 / k + "px");
}



function save(){
	var svgString = getSVGString(svg.node());
	svgString2Image( svgString, 2*800, 2*500, 'png', save ); // passes Blob and filesize String to the callback

	function save( dataBlob, filesize ){
		saveAs( dataBlob, 'D3 vis exported to PNG.png' ); // FileSaver.js function
	}
};

function getSVGString( svgNode ) {
	svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
	var cssStyleText = getCSSStyles( svgNode );
	appendCSS( cssStyleText, svgNode );

	var serializer = new XMLSerializer();
	var svgString = serializer.serializeToString(svgNode);
	svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
	svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

	return svgString;

	function getCSSStyles( parentElement ) {
		var selectorTextArr = [];

		// Add Parent element Id and Classes to the list
		selectorTextArr.push( '#'+parentElement.id );
		for (var c = 0; c < parentElement.classList.length; c++)
				if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
					selectorTextArr.push( '.'+parentElement.classList[c] );

		// Add Children element Ids and Classes to the list
		var nodes = parentElement.getElementsByTagName("*");
		for (var i = 0; i < nodes.length; i++) {
			var id = nodes[i].id;
			if ( !contains('#'+id, selectorTextArr) )
				selectorTextArr.push( '#'+id );

			var classes = nodes[i].classList;
			for (var c = 0; c < classes.length; c++)
				if ( !contains('.'+classes[c], selectorTextArr) )
					selectorTextArr.push( '.'+classes[c] );
		}

		// Extract CSS Rules
		var extractedCSSText = "";
		for (var i = 0; i < document.styleSheets.length; i++) {
			var s = document.styleSheets[i];
			
			try {
			    if(!s.cssRules) continue;
			} catch( e ) {
		    		if(e.name !== 'SecurityError') throw e; // for Firefox
		    		continue;
		    	}

			var cssRules = s.cssRules;
			for (var r = 0; r < cssRules.length; r++) {
				if ( contains( cssRules[r].selectorText, selectorTextArr ) )
					extractedCSSText += cssRules[r].cssText;
			}
		}
		

		return extractedCSSText;

		function contains(str,arr) {
			return arr.indexOf( str ) === -1 ? false : true;
		}

	}

	function appendCSS( cssText, element ) {
		var styleElement = document.createElement("style");
		styleElement.setAttribute("type","text/css"); 
		styleElement.innerHTML = cssText;
		var refNode = element.hasChildNodes() ? element.children[0] : null;
		element.insertBefore( styleElement, refNode );
	}
}
function svgString2Image( svgString, width, height, format, callback ) {
	var format = format ? format : 'png';

	var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to data URL

	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");

	canvas.width = width;
	canvas.height = height;

	var image = new Image();
	image.onload = function() {
		context.clearRect ( 0, 0, width, height );
		context.drawImage(image, 0, 0, width, height);

		canvas.toBlob( function(blob) {
			var filesize = Math.round( blob.length/1024 ) + ' KB';
			if ( callback ) callback( blob, filesize );
		});

		
	};

	image.src = imgsrc;
}